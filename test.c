#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "fifo.h"

const int N_RELAYS = 10;
const int TEST_SIZE = 1000000;
const int FIFO_SIZE = 1000;

typedef struct {
    fifo* first;
    fifo* second;
} fifo_pair;

void push_numbers(fifo *dest) {
    // Test with invalid pointers.
    void* item = NULL;
    for (int i=1; i<TEST_SIZE; i++) {
        fifo_push(dest, ++item);
    }
}

void relay_messages(fifo_pair *fifos) {
    while(1) {
        fifo_push(fifos->second, fifo_pop(fifos->first));
    }
}

void pull_numbers(fifo *source) {
    void* item = NULL;
    for (int i=1; i<TEST_SIZE; i++) {
        void* result = fifo_pop(source);
        if (result != ++item) {
            printf("Expected %p, got %p\n", item, result);
        }
    }
    printf("Success\n");
}

int main(int argc, char* argv[]) {
    fifo_pair fifos;
    fifos.first = fifo_new(FIFO_SIZE);
    if (fifos.first == NULL) {
        printf("Failed to create FIFO\n");
        return 1;
    }
    fifos.second = fifo_new(FIFO_SIZE);
    if (fifos.second == NULL) {
        printf("Failed to create FIFO\n");
        return 1;
    }
    pthread_t producer, relays[N_RELAYS], consumer;

    signal_setup();
    if (pthread_create(&producer, NULL,
                (void *) push_numbers,
                fifos.first)) {
        printf("Failed to create producer.\n");
        return 1;
    }

    for (int relay=0; relay<N_RELAYS; relay++) {
        if (pthread_create(&relays[relay], NULL,
                    (void *) relay_messages,
                    &fifos)) {
            printf("Failed to create relay %i.\n", relay);
            return 1;
        }
    }

    if (pthread_create(&consumer, NULL,
                (void *) pull_numbers,
                fifos.second)) {
        printf("Failed to create consumer.\n");
        return 1;
    }

    pthread_join(producer, NULL);
    pthread_join(consumer, NULL);
    return 0;
}
