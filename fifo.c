/*
 * Lock free FIFO implementation in C.
 * Hopefully threadsafe but needs testing.
 *
 * By Graham Breed.
 *
 * Do what you like with it.
 *
 */

#include <stdlib.h>
#include <signal.h>
#include <sched.h>
#include <stdio.h>
#include "fifo.h"

// How many pushers or poppers can be waiting.
// Can be generous because the FIFOs have low overhead.
const int MAX_SUBSCRIBERS = 100;

const long WAIT_TIME_NS = 500000; // nanoseconds

/* These are, or were, used by the thread library.
 * As most of that isn't used here,
 * hopefully it's safe to use them.
 */
const int SIGPUSHED = SIGUSR1;
const int SIGPOPPED = SIGUSR2;

/* Global objects that can be treated as constants.
 * Must be initialized and never change afterwards.
 */
sigset_t POPMASK;
sigset_t PUSHMASK;
struct timespec WAIT_TIME;

// Friendlier alias for the atomic operation.
#define test_and_set(a,b,c) __sync_bool_compare_and_swap(&(a),b,c)

/* The asynchronous FIFO used as the basis of the high-level one.
 */
async_fifo* async_fifo_new(int size) {
    async_fifo* self = malloc(sizeof(async_fifo));
    if (self) {
        self->size = size;
        self->queue = calloc(size, sizeof(void *));
        if (self->queue == NULL) {
            free(self);
            return NULL;
        }
        // Add to the tail, remove from the head.
        self->head = 0;
        self->tail = 1;
        for (int i=0; i<size; i++) {
            self->queue[i] = NULL;
        }
    }
    return self;
}

int async_fifo_dec_index(async_fifo* self, int index) {
    return index? index - 1: self->size - 1;
}

void* async_fifo_pop(async_fifo* self) {
    void* result;
    while ((result = self->queue[self->head])) {
        // Keep trying while the FIFO doesn't look empty.
        int old_head = self->head;
        int new_head = async_fifo_dec_index(self, old_head);
        if (test_and_set(self->head, old_head, new_head)) {
            if (test_and_set(self->queue[old_head], result, NULL)) {
                return result;
            }
            else {
                // This really shouldn't happen.
                printf("Pop claimed but contents altered.\n");
                self->head = old_head;
            }
        }
    }
    return result;
}

// Returns 0 on success
int async_fifo_push(async_fifo* self, void* item) {
    while(1) {
        int old_tail = self->tail;
        int new_tail = async_fifo_dec_index(self, old_tail);
        if (self->queue[new_tail]) {
            // FIFO looks full.  Return failure.
            return 1;
        }
        if (test_and_set(self->tail, old_tail, new_tail)) {
            if (test_and_set(self->queue[new_tail], NULL, item)) {
                return 0;
            }
            else {
                // This really shouldn't happen.
                printf("Push claimed but contents altered.\n");
                self->tail = old_tail;
            }
        }
    }
}

void async_fifo_delete(async_fifo* self) {
    free(self->queue);
    free(self);
}

/*
 * The blocking FIFO for high-level use.
 */

/*
 * Initialize global state.
 */
void signal_setup() {
    // Block the signals we're sending so we don't crash.
    sigset_t signal_mask;
    sigemptyset(&signal_mask);
    sigaddset(&signal_mask, SIGPOPPED);
    sigaddset(&signal_mask, SIGPUSHED);
    pthread_sigmask(SIG_BLOCK, &signal_mask, NULL);

    // POP the global signal masks.
    sigemptyset(&PUSHMASK);
    sigaddset(&PUSHMASK, SIGPUSHED);
    sigemptyset(&POPMASK);
    sigaddset(&POPMASK, SIGPOPPED);

    // Initialize the timeout for signal waiting.
    WAIT_TIME.tv_sec = 0;
    WAIT_TIME.tv_nsec = WAIT_TIME_NS;
}

/* Make a new blocking FIFO.
 * Call signal_POP() before this.
 */
extern fifo* fifo_new(int size) {
    fifo* self = malloc(sizeof(fifo));
    if (self == NULL) {
        return NULL;
    }
    self->impl = async_fifo_new(size);
    if (self->impl == NULL) {
        free(self);
        return NULL;
    }
    self->poppers = async_fifo_new(MAX_SUBSCRIBERS);
    if (self->poppers == NULL) {
        free(self->impl);
        free(self);
        return NULL;
    }
    self->pushers = async_fifo_new(MAX_SUBSCRIBERS);
    if (self->pushers == NULL) {
        free(self->poppers);
        free(self->impl);
        free(self);
        return NULL;
    }
    return self;
}

extern void* fifo_pop(fifo* self) {
    pthread_t thread_id = pthread_self();
    while (1) {
        void* result = async_fifo_pop(self->impl);
        if (result == NULL) {
            // FIFO empty.
            // Risky pointer leak . . .
            async_fifo_push(self->poppers, &thread_id);
            sched_yield();
            sigtimedwait(&PUSHMASK, NULL, &WAIT_TIME);
        }
        else {
            pthread_t* pusher = async_fifo_pop(self->pushers);
            if (pusher) {
                pthread_kill(*pusher, SIGPOPPED);
            }
            return result;
        }
    }
}

extern void fifo_push(fifo* self, void* item) {
    pthread_t thread_id = pthread_self();
    while (async_fifo_push(self->impl, item)) {
        // FIFO full or race.
        async_fifo_push(self->pushers, &thread_id);
        sched_yield();
        sigtimedwait(&POPMASK, NULL, &WAIT_TIME);
    }
    pthread_t* popper = async_fifo_pop(self->poppers);
    if (popper) {
        pthread_kill(*popper, SIGPUSHED);
    }
}

extern void fifo_delete(fifo* self) {
    async_fifo_delete(self->impl);
    async_fifo_delete(self->pushers);
    async_fifo_delete(self->poppers);
    free(self);
}
