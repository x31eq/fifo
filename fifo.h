#include <pthread.h>

typedef struct {
    void** queue;
    int    size;
    int    head, tail;
} async_fifo;

typedef struct {
    async_fifo *impl, *poppers, *pushers;
} fifo;

extern fifo* fifo_new(int size);
extern void  signal_setup();
extern void* fifo_pop(fifo* self);
extern void  fifo_push(fifo* self, void* item);
extern void  fifo_delete(fifo* self);
